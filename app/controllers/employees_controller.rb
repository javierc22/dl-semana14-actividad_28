class EmployeesController < ApplicationController
  
  def create
    @employee = Employee.new(employee_params)
    
    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee.company, notice: 'El empleado ha sido creado.' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    @employee = Employee.find(params[:id])
    @employee.destroy
    redirect_to @employee.company, notice: 'El empleado fue eliminado' 
  end

  private
  def employee_params
    params.require(:employee).permit(:first_name, :last_name, :email, :company_id, :area_id)
  end
end
